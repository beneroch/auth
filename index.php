<?php
session_start();
require_once __DIR__ . '/vendor/autoload.php';

use \Beneroch\Auth\Auth;

$config = file_get_contents('config.json');
$config = json_decode($config, true);
$config = $config['oauth'];

$googleCfg = $config['google'];
$facebookCfg = $config['facebook'];
$twitterCfg = $config['twitter'];

$facebook = new Auth('facebook', $facebookCfg);
$loginUrl = $facebook->loginUrl();

$google = new Auth('google', $googleCfg);
$loginUrlGoogle = $google->loginUrl();

$twitter = new Auth('twitter', $twitterCfg);
$loginUrlTwitter = $twitter->loginUrl();

echo '<a href="'.$loginUrl.'">Login with facebook!</a>';
echo '<br/>';
echo '<a href="'.$loginUrlGoogle.'">Login with google!</a>';
echo '<br/>';
echo '<a href="'.$loginUrlTwitter.'">Login with Twitter!</a>';

$google->authenticate();
$facebook->authenticate();
$twitter->authenticate();

var_dump($google->user());
var_dump($facebook->user());
var_dump($twitter->user());
