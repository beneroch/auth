<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/Auth.php';


$config = file_get_contents('config.json');
$config = json_decode($config, true);
$config = $config['oauth'];
$facebookCfg = $config['facebook'];
$facebook = new Auth('facebook', $facebookCfg);

$user = $facebook->authenticate();
// var_dump($user);
