<?php
namespace Beneroch\Auth;

use \Exception;
use \Facebook\Facebook;
use \Abraham\TwitterOAuth\TwitterOAuth;

class Auth
{
	/**
	 * Google, Facebook, Twitter
	 * @var [type]
	 */
	protected $type;

	/**
	 * Access token
	 * @var [type]
	 */
	protected $accessToken;

	/**
	 * Twitter friendly...
	 * Thats how TwitterOAuth works.
	 * @var string
	 * @var string
	 */
	protected $requestToken;
	protected $requestSecretToken;

	/**
	 * Outputable user
	 * @var array $user
	 */
	protected $user;


	/**
	 * App related content
	 *
	 * @see doc for google, facebook, twitter, etc.
	 * @var string $appId
	 * @var string $appSecret
	 * @var string $appRedirect
	 * @var string $appVersion 	| Facebook only
	 * @var string $appName 	| Google only
	 * @var array  $appScope
	 * @var object $app
	 */
	protected $appId;
	protected $appSecret;
	protected $appRedirect;
	protected $appVersion;
	protected $appName;
	protected $appScope;
	protected $app;

	/**
	 * When error occurs, send it right here
	 * @var mixed
	 */
	protected $errors;

	/**
	 * Requirements by types
	 * Supported type are:
	 * Twitter
	 * Google
	 * Facebook
	 *
	 * @var array
	 */
	private $available_types = [
		'facebook' => [
			'classNames' => ['\Facebook\Facebook'],
			'requirements' => [
				'appId',
				'appSecret',
				'appVersion',
				'appRedirect'
			]
		],
		'google' => [
			'classNames' => ['Google_Client', 'Google_Service_Oauth2'],
			'requirements' => [
				'appId',
				'appSecret',
				'appName',
				'appRedirect',
				'appScope'
			]
		],
		'twitter' => [
			'classNames' => ['\Abraham\TwitterOAuth\TwitterOAuth'],
			'requirements' => [
				'appId',
				'appSecret',
				'appName',
				'appRedirect'
			]
		]
	];

	/**
	 * Options:
	 * appId
	 * appSecret
	 * appVersion // Facebook
	 * appRedirect // Watch the APP SETTINGS and make sure it matches (escpescially facebook)
	 * appName
	 * appScope
	 *
	 * @param string 	$type    google | twitter | facebook
	 * @param array 	$options AppId, etc.
	 */
	public function __construct($type, $options=null)
	{
		if (!$type) {
			return false;
		}
		$this->setType($type);

		if (!$options) {
			return $this;
		}
		if (!is_array($options)) {
			// Unnecessary
			return $this;
		}

		// Magic setter from options
		foreach ($options as $key => $val) {
			$key = 'set'.ucfirst($key);
			if (is_callable([$this, $key])) {
				call_user_func([$this, $key], $val);
			}
		}
		return $this;
	}

	/**
	 * Return formatted user
	 * id
	 * name
	 * picture (if avialable)
	 *
	 * @return array user
	 */
	public function authenticate()
	{
		$type = $this->type();
		$user = null;

		switch ($type) {
			case 'facebook' :
				$user = $this->authenticateFacebook();
			break;

			case 'google' :
				$user = $this->authenticateGoogle();
			break;

			case 'twitter' :
				$user = $this->authenticateTwitter();
			break;
		}

		// So that user is callable at all time from the outside world.
		$this->setUser($user);

		return $user;
	}

	/**
	 * Return facebook user with avaialable informations
	 *
	 * @see $this->getUser();
	 * @return array user
	 */
	public function authenticateFacebook()
	{
		$app = $this->app();

		// Session.
		if (isset($_SESSION['facebook_oauth_token'])) {
			$app->setDefaultAccessToken($_SESSION['facebook_oauth_token']);
			$user = $this->getUser();
			if (!$user) {
				unset($_SESSION['facebook_oauth_token']);
			}
			return $user;
		}

		$helper = $app->getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();
			if (isset($accessToken)) {
				$oAuth2Client = $app->getOAuth2Client();

				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
				$app->setDefaultAccessToken($longLivedAccessToken);
				$_SESSION['facebook_oauth_token'] = (string)$longLivedAccessToken;
			}

		} catch(\Exception $e) {
			// Something WRONG happened.
			$this->setErrors($e);
		}

		return $this->getUser();

	}

	/**
	 * Return google user with avialble informations
	 *
	 * @see $this->getUser();
	 * @return [type] [description]
	 */
	public function authenticateGoogle()
	{
		$app = $this->app();
		$user = [];

		if (isset($_SESSION['google_oauth_token'])) {
			$app->setAccessToken($_SESSION['google_oauth_token']);
			$user = $this->getUser();
			if (!$user) {
				unset($_SESSION['google_oauth_token']);
			}
			return $user;
		}

		if (isset($_GET['code'])) {
			$code = $_GET['code'];
		}

		if (!isset($code)) {
			// Error.
			$this->setErrors([ 'code' => 'There is just no code defined']);
			return $this->getUser();
		}

		$app->authenticate($code);
		$token = $app->getAccessToken();
		if ($token) {
			$_SESSION['google_oauth_token'] = $token;
		}

		$user = $this->getUser();

		return $user;

	}

	/**
	 * Return twitter user with avialble informations
	 *
	 * @see $this->getUser();
	 * @return [type] [description]
	 */
	public function authenticateTwitter()
	{
		$app = $this->app();

		if ($this->getUser()) {
			return $this->getUser();
		}

		if (isset($_GET['oauth_token']) || isset($_GET['oauth_verifier'])) {
			$token = $_GET['oauth_token'];
			$verifier = $_GET['oauth_verifier'];
		}

		// No get parameter token means we need to try something else
		if (!isset($token)) {
			return $this->getUser();
		}

		try {
			$app->setOauthToken($token, $verifier);
			$accessToken = $app->oauth("oauth/access_token", ["oauth_verifier" => $verifier]);

			// Keep
			$oauth = $accessToken['oauth_token'];
			$oauthSecret = $accessToken['oauth_token_secret'];
			$_SESSION['twitter_oauth_token'] = $oauth;
			$_SESSION['twitter_oauth_token_secret'] = $oauthSecret;

			$connection = new TwitterOAuth($this->appId(), $this->appSecret(), $oauth, $oauthSecret);
			$this->setApp($connection);

		} catch (\Exception $exception) {
			// Something went wrong.
			$this->setErrors($exception);
		}
		$user = $this->getUser();

		return $user;
	}

	/**
	 * Return formatted user
	 *
	 * @return array $user
	 */
	public function getUser()
	{
		$type = $this->type();
		$app = $this->app();

		switch ($type) {
			case 'twitter':
				try {
					$credentials = $app->get("account/verify_credentials");
					if (isset($credentials->errors)) {
						return null;
					}
					$user = [
						'id' => $credentials->id,
						'name' => $credentials->name,
						'screen_name' => $credentials->screen_name,
						'image' => $credentials->profile_image_url_https
					];
					return $user;

				} catch(\Exception $e) {
					$this->setErrors($e);
					return null;
				}
			break;

			case 'facebook':
				try {
					$response = $app->get('/me');
					$userNode = $response->getGraphUser();
					$user = [
						'id' => $userNode->getId(),
						'name' => $userNode->getName()
					];
					return $user;
				} catch(\Exception $e) {
					$this->setErrors($e);
					return null;
				}
			break;

			case 'google' :
				try {
					$obj = $app->verifyIdToken();
					$user = [
						'name' => $obj['name'],
						'id' => $obj['sub'],
						'picture' => $obj['picture']
					];
					return $user;
				}
				catch (\Exception $e) {
					$this->setErrors($e);
					return null;
				}
			break;
		}
	}

	/**
	 * Remove all session vars
	 *
	 * @return $this
	 */
	public static function logout()
	{
		unset(
			$_SESSION['google_oauth_token'],
			$_SESSION['twitter_oauth_token'],
			$_SESSION['twitter_oauth_token_secret'],
			$_SESSION['facebook_oauth_token']
		);
	}

	/**
	 * Type configs and requirements
	 * @return array
	 */
	public function cfg()
	{
		return $this->available_types[ $this->type() ];
	}

	/**
	 * [init description]
	 * @return [type] [description]
	 */
	public function init()
	{
		if ($this->app) {
			return false;
		}
		// Everything okay?
		// Throw exception if not
		$this->checkRequirements();

		// Go ahead
		$this->createApp();
	}

	/**
	 * Get the login URL for current type
	 *
	 * @return string URL
	 */
	public function loginUrl()
	{
		$type = $this->type();
		$app = $this->app();

		switch ($type) {
			case 'facebook':
				$redirect = $this->appRedirect();
				$scopes = $this->appScope();
				$service = $app->getRedirectLoginHelper();

				if (!$scopes) {
					$scopes = [];
				}
				return $service->getLoginUrl($redirect, $scopes);
			break;

			case 'google':
				return $app->createAuthUrl();
			break;

			case 'twitter':
				return $app->url('oauth/authorize', array('oauth_token' => $this->requestToken()));
			break;
		}

		// Default
		return '';
	}

	/**
	 * When app is first called, creates the
	 * app based on type and options
	 *
	 * @return Facebook||Twitter||Google App
	 */
	public function createApp()
	{
		$type = $this->type();
		if (!$type) {
			// Error.
			return false;
		}

		switch ($type) {
			case 'facebook':
				$fb = new \Facebook\Facebook([
				  'app_id' => $this->appId(),
				  'app_secret' => $this->appSecret(),
				  'default_graph_version' => $this->appVersion(),
				]);
				$this->setApp($fb);
			break;

			case 'google' :
				$client = new \Google_Client();
				$client->setApplicationName($this->appName());
				$client->setClientId($this->appId());
				$client->setClientSecret($this->appSecret());
				$client->setRedirectUri($this->appRedirect());

				$scopes = $this->appScope();
				if (is_array($scopes)) {
					foreach ($scopes as $val) {
						$client->addScope($val);
					}
				}
				$this->setApp($client);
			break;

			case 'twitter' :
				$token = null;
				$verifier = null;
				if (isset($_SESSION['twitter_oauth_token']) && isset($_SESSION['twitter_oauth_token_secret'])) {
					$token = $_SESSION['twitter_oauth_token'];
					$verifier = $_SESSION['twitter_oauth_token_secret'];
				}

				try {
					$connection = new TwitterOAuth($this->appId(), $this->appSecret(), $token, $verifier);
				} catch (\Exception $exception) {
					// Something went wrong.
					$this->setErrors($exception);
				}

				// If no token set in SESSION, try to get a temporary one
				if (!$token) {
					$token = $connection->oauth('oauth/request_token', array('oauth_callback' => $this->appRedirect()));
					$this->setRequestToken($token['oauth_token']);
					$this->setRequestSecretToken($token['oauth_token_secret']);
				}
				$this->setApp($connection);
			break;
		}
	}


	/**
	 * Check if all requirements are set
	 *
	 * @return $this
	 */
	public function checkRequirements()
	{
		$cfg = $this->cfg();
		$requirements = $cfg['requirements'];

		foreach ($requirements as $r) {
			$func = [ $this, $r ];
			if (!is_callable($func)) {
				// That is strange.
				// Invalid requirements
				continue;
			}

			$val = call_user_func($func);
			if (!$val) {
				// Missing parameters
				throw new Exception(
					'Missing informations for ' . $this->type() . ' with: ' . $r . ' : ' . $val
				);
				return $this;
			}
		}


		$classNames = $cfg['classNames'];
		foreach ($classNames as $className) {
			if (!class_exists($className)) {
				// Error
				throw new Exception(
					'Missing class: ' . $className . ' Try composer install'
				);
				return $this;
			}
		}


		return $this;
	}

/*
 * GETTERS
 */
	public function type() 			{ return $this->type; }
	public function appId() 		{ return $this->appId; }
	public function appSecret() 	{ return $this->appSecret; }
	public function appRedirect() 	{ return $this->appRedirect; }
	public function appVersion() 	{ return $this->appVersion; }
	public function appName() 		{ return $this->appName; }
	public function appScope() 		{ return $this->appScope; }
	public function accessToken()	{ return $this->accessToken; }

	// This one is twitter friendly
	public function requestToken()	{ return $this->requestToken; }
	public function requestSecretToken()	{ return $this->requestSecretToken; }
	public function user() { return $this->user; }
	public function app() {
		if (!$this->app) {
			$this->init();
		}
		return $this->app;
	}
	public function errors() { return $this->errors; }



/*
 * SETTERS
 */
	public function setType($type=null)
	{
		if (!isset($this->available_types[$type])) {
			// Error
			return false;
		}

		$this->type = $type;
		return $this;
	}
	public function setAppId($appId)
	{
		$this->appId = $appId;
		return $this;
	}
	public function setAppSecret($appSecret)
	{
		$this->appSecret = $appSecret;
		return $this;
	}
	public function setAppRedirect($appRedirect)
	{
		$this->appRedirect = $appRedirect;
		return $this;
	}
	public function setAppVersion($appVersion)
	{
		$this->appVersion = $appVersion;
		return $this;
	}
	public function setAppName($appName)
	{
		$this->appName = $appName;
		return $this;
	}
	public function setAppScope($appScope)
	{
		if (is_string($appScope)) {
			$appScope = [$appScope];
		}
		$this->appScope = $appScope;
		return $this;
	}
	public function setApp($app)
	{
		$this->app = $app;
		return $this;
	}
	public function setAccessToken($accessToken)
	{
		$this->accessToken = $accessToken;
		return $this;
	}
	public function setRequestToken($requestToken)
	{
		$this->requestToken = $requestToken;
		return $this;
	}
	public function setRequestSecretToken($requestSecretToken)
	{
		$this->requestSecretToken = $requestSecretToken;
		return $this;
	}
	public function setUser($user)
	{
		$this->user = $user;
	}
	public function setErrors($errors)
	{
		$this->errors = $errors;
	}

	/**
	 * Add a scope.
	 * @param string $appScope
	 */
	public function addAppScope(String $appScope)
	{
		if (!is_string($appScope)) {
			// Error.
			return false;
		}
		if (!is_array($this->appScope)) {
			$this->appScope = [$appScope];
		} else {
			$this->appScope[] = $appScope;
		}
	}
}
