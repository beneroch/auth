Social websites login helper
============================

# Facebook


## Step 1: Create app
Go to https://developers.facebook.com/apps
Create the app you want. (Sign In Test App)
Remember APP ID and APP SECRET AND Redirect URI
Remember app name

# Google
Go to https://console.developers.google.com/home/dashboard
Create the app you want. (Sign In Test App) -id: sign-in-test-app
Remember APP ID and APP SECRET...
Remember app name


# Twitter
Go to https://apps.twitter.com/
Create APP
Remember APP ID and APP SECRET...
Remember app name
